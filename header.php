<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>Title</title>


    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!--<style type="text/css">
    body,
    img,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    form,
    table,
    td,
    ul,
    ol,
    li,
    dl,
    dt,
    dd,
    pre,
    blockquote,
    fieldset,
    label {
        margin: 0;
        padding: 0;
        border: 0;
    }

    body {
        background-color: #777;
        border-top: solid 10px #7b94b2;
        font: 90% Arial, Helvetica, sans-serif;
        padding: 20px;
    }

    h1,
    h2,
    h3 {
        margin: 10px 0;
    }

    h1 {}

    h2 {
        color: #f66;
    }

    h3 {
        color: #6b84a2;
    }

    p {
        margin: 10px 0;
    }

    a {
        color: #7b94b2;
    }

    ul,
    ol {
        margin: 10px 0 10px 40px;
    }

    li {
        margin: 4px 0;
    }

    dl.defs {
        margin: 10px 0 10px 40px;
    }

    dl.defs dt {
        font-weight: bold;
        line-height: 20px;
        margin: 10px 0 0 0;
    }

    dl.defs dd {
        margin: -20px 0 10px 160px;
        padding-bottom: 10px;
        border-bottom: solid 1px #eee;
    }

    pre {
        font-size: 12px;
        line-height: 16px;
        padding: 5px 5px 5px 10px;
        margin: 10px 0;
        background-color: #e4f4d4;
        border-left: solid 5px #9EC45F;
        overflow: auto;
        tab-size: 4;
        -moz-tab-size: 4;
        -o-tab-size: 4;
        -webkit-tab-size: 4;
    }

    .wrapper {
        background-color: #ffffff;
        width: 800px;
        border: solid 1px #eeeeee;
        padding: 20px;
        margin: 0 auto;
    }

    #tabs {
        margin: 20px -20px;
        border: none;
    }

    #tabs,
    #ui-datepicker-div,
    .ui-datepicker {
        font-size: 85%;
    }

    .clear {
        clear: both;
    }

    .example-container {
        background-color: #f4f4f4;
        border-bottom: solid 2px #777777;
        margin: 0 0 20px 40px;
        padding: 20px;
    }

    .example-container input {
        border: solid 1px #aaa;
        padding: 4px;
        width: 175px;
    }

    .ebook {}

    .ebook img.ebookimg {
        float: left;
        margin: 0 15px 15px 0;
        width: 100px;
    }

    .ebook .buyp a iframe {
        margin-bottom: -5px;
    }
</style>-->

    <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" media="all" type="text/css" href="assets/css/jquery-ui-timepicker-addon.css" />



    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">


</head>

<body>
    <!--PRELOADER START-->

    <!--    [ Strat Preloader Area]-->
    <!--<div class="pre-loader-area">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>-->


    <!--    [Finish Poreloader Area]-->
    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat Logo Area]-->
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->
